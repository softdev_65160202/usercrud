
package com.mycompany.usermanagementproject;

import java.util.ArrayList;

public class UserService {
    private ArrayList<User> userList;
    private int lastId = 1;
    
    public UserService(){
        userList = new ArrayList<User>();
    }
    
    public User addUser(User newUser){
        newUser.setId(lastId++);
        userList.add(newUser);
        return newUser;
    }
    
    public User getUser(int index){
        return userList.get(index);
    }
    
    public ArrayList<User> getUsers(){
        return userList;
    }
    
    public int getSize(){
        return userList.size();
    }
    
    public void logUserList(){
        for(User u : userList){
            System.out.println(u);
        }
    }

    User updateUser(int index, User updatedUser) {
        User user = userList.get(index);
        user.setLogin(updatedUser.getLogin());
        user.setName(updatedUser.getName());
        user.setPassword(updatedUser.getPassword());
        user.setGender(updatedUser.getGender());
        user.setRole(updatedUser.getRole());
        return userList.get(index);
    }

    User deleteUser(int index) {
        return userList.remove(index);
    }
}
